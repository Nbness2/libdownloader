import asyncore
import OPCODES
from test_packages_2.ppbu.BufferUtils import ByteBuffer


class ClientHandler(asyncore.dispatcher):

    def __init__(self, addr, repo_ref):

        self.addr = addr[1]
        asyncore.dispatcher.__init__(self, addr[0])
        print("Initiated transfer session with", self.addr)
        self.write_data = []
        self.buffer_size = 512
        self.repo = repo_ref
        self.current_package = None

    def handle_write(self):

        data = self.write_data.pop()
        sent = self.send(data[:self.buffer_size])

        if sent < len(data):
            remaining = data[sent:]
            self.write_data.append(remaining)

    def write(self, data):
        self.write_data.append(data)

    def handle_read(self):
        buffer = ByteBuffer()

        opcode = int.from_bytes(self.recv(1), byteorder="big")
        print("op:", opcode)
        if opcode == OPCODES.Client.CONFIRM_STATUS:
            return
        elif opcode == OPCODES.Client.REQUEST_PKG_META:  # request pkg meta
            print("requesting pkg")
            package_name_size = int.from_bytes(self.recv(1), byteorder="big")
            package_name = self.recv(package_name_size).decode()
            print("requesting pkg meta for", package_name)
            if self.current_package:
                print("err: pkg already in session")
                buffer.write_byte(OPCODES.Server.DENY_STATUS)
                buffer.write_byte(OPCODES.Client.REQUEST_PKG_META)
                buffer.write_byte(1)  # already doing a package session
                self.write(buffer.get_buffer())
            if package_name in self.repo:
                self.current_package = self.repo[package_name]
                self.write(self.current_package.build_meta_packet())
                print("package meta wrote to socket")
            else:
                print("err: package does not exist")
                buffer.write_byte(OPCODES.Server.DENY_STATUS)
                buffer.write_byte(OPCODES.Client.REQUEST_PKG_META)
                buffer.write_byte(0)  # package does not exist
                self.write(buffer.get_buffer())  # package does not exist

        elif opcode == OPCODES.Client.REQUEST_DESC_CHUNK:  # request desc chunk
            # S -> [opcode (1b), description_chunk_id (2b)] - 3 bytes
            desc_chunk_id = int.from_bytes(self.recv(2), byteorder="big")
            try:
                # S -> [opcode (1b), chunk_id (2b), chunk_length (2b), chunk_contents (1-512b)] - 6 to 517 bytes
                desc_chunk_packet = self.current_package.build_desc_chunk_packet(desc_chunk_id)
                self.write(desc_chunk_packet)
            except IndexError:
                buffer.write_byte(OPCODES.Server.DENY_STATUS)
                buffer.write_byte(OPCODES.Client.REQUEST_DESC_CHUNK)
                buffer.write_byte(0)  # bad chunk id
                self.write(buffer.get_buffer())

    def writable(self):

        return bool(self.write_data)

    def handle_close(self):

        self.close()
        print(self.addr, "closed")
