import socket


def echo_client(port):

    conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    conn.connect(('127.0.0.1', port))

    while True:
        message = input("Message to send: ")

        if message == "~~":
            conn.close()
            return
        print('sending: %r' % message)
        conn.send(message.encode())
        data = conn.recv(512).decode()
        print('recv: %r' % data)

echo_client(1337)
