from Structures.Server.Package import Package
import os


class Repo:
    def __init__(self):
        self.packages = []

    def __getitem__(self, item):

        for package in self.packages:

            if item == package.package_name:
                return package

        return False

    def __contains__(self, item):

        for package in self.packages:

            if item == package.package_name:
                return True

        return False

    def build_packages(self):
        package_paths = []
        ignore_packages = []

        with open("config.cfg", "r") as config_file:
            data = config_file.readlines()

        for line in data:
            config_name, config_value = line.strip().split("=")
            config_name = config_name.strip().lower()
            config_value = config_value.strip()

            if config_name == "ignore_packages":

                if config_value == "[]":
                    continue

                ignore_packages = config_value.split("\"")

                for value in ignore_packages:

                    if len(value) < 3 and value in ("[", "[ ", " [", "]", "] ", " ]", ",", ", ", " ,"):
                        ignore_packages.remove(value)

            if config_name == "package_paths":

                if config_value == "[]":
                    continue

                package_paths = config_value.split("\"")

                for value in package_paths:

                    if len(value) < 3 and value in ("[", "[ ", " [", "]", "] ", " ]", ",", ", ", " ,"):
                        package_paths.remove(value)

        for package_path in package_paths:
            found_possible_package_names = next(os.walk(package_path))[1]

            for ignore_package_name in ignore_packages:

                if ignore_package_name in found_possible_package_names:
                    found_possible_package_names.remove(ignore_package_name)

            for package_name in found_possible_package_names:
                package = Package(
                    package_name=package_name,
                    package_path=package_path + "\\" + package_name
                )
                self.packages.append(package)

        self.packages = tuple(self.packages)

    def initialize_packages(self):

        for package in self.packages:
            package.scan()


if __name__ == "__main__":
    repo = Repo()
    repo.build_packages()
    repo.initialize_packages()
    repo["ppll"].flatten_contents()
    print(repo["ppll"][0])
