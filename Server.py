import socket
import asyncore

from ClientHandler import ClientHandler
from Repo import Repo


class AsyncServer(asyncore.dispatcher):

    def __init__(self, address, handler_class, max_connections=10):

        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.set_reuse_addr()
        self.bind(address)
        self.addr = self.socket.getsockname()
        self.listen(max_connections)
        self.handler_class = handler_class
        self.package_repo = Repo()

    def handle_accept(self):

        client_data = self.accept()

        if client_data:
            print("server handling", client_data)
            self.handler_class(client_data, self.package_repo)


if __name__ == "__main__":
    host = 'localhost'
    port = 1337
    server = AsyncServer((host, port), ClientHandler)
    server.package_repo.build_packages()
    server.package_repo.initialize_packages()
    asyncore.loop()
