

def chunk_iterable(iterable, chunk_size=1):

    l = len(iterable)

    for ndx in range(0, l, chunk_size):
        yield iterable[ndx:min(ndx + chunk_size, l)]


def length(data):

    if type(data) is int:
        return int(data.bit_length() + 7) // 8

    else:
        return len(data)


def replace_str_char(string, idx, replacement):

    if type(string) is str:
        replaced = '{}{}{}'.format(string[:idx], replacement, string[idx + 1:])

    elif type(string) is bytes:
        replaced = b''.join([string[:idx], replacement, string[idx + 1:]])

    else:
        raise TypeError("Cannot replace a character in type {}".format(type(string)))
    return replaced


def confirm_data_type(data, expected_type, field_name):

    if type(data) == expected_type:
        return data
    raise TypeError("Invalid type ({}) for field {}, must be {}".format(type(data), field_name, expected_type))
