from Reference import Reference
from SingleLinkedList import SingleLinkedList


class DoubleLinkedList(SingleLinkedList):

    def __init__(self, iterable=()):
        SingleLinkedList.__init__(self, iterable)

    def __getitem__(self, item_idx, set_item=False, set_val=None, remove_item=False):

        length = len(self)
        left_to_right = False
        i = 0

        if (item_idx > length-1) or (item_idx < 0-length):
            raise IndexError('{} index out of range'.format(self.__class__.__name__))

        while True:

            if item_idx >= 0:

                if item_idx >= length//2:
                    item_idx = abs(item_idx - length + 1)
                    current_ref = self.tail

                else:
                    left_to_right = True
                    current_ref = self.head
                break

            else:
                item_idx = abs(item_idx + length)
        del length

        while current_ref:

            if i == item_idx:
                data_object = current_ref.value

                if set_item:
                    current_ref.value = set_val

                elif remove_item:

                    if current_ref.last_ref and current_ref.next_ref:
                        current_ref.last_ref.next_ref = current_ref.next_ref
                        current_ref.next_ref.last_ref = current_ref.last_ref

                    else:

                        if left_to_right:
                            self.head = current_ref.next_ref

                            if current_ref.next_ref:
                                current_ref.next_ref.last_ref = None

                        else:
                            self.tail = current_ref.last_ref

                            if current_ref.last_ref:
                                current_ref.last_ref.next_ref = None
                    del current_ref
                return data_object

            else:

                if left_to_right:
                    current_ref = current_ref.next_ref

                else:
                    current_ref = current_ref.last_ref
                i += 1

    def __setitem__(self, item_idx, data_object):

        self.__getitem__(item_idx, True, data_object)

    def __delitem__(self, item_idx):

        return self.__getitem__(item_idx, remove_item=True)

    def _remove(self, item_idx, length=None):

        return self.__getitem__(item_idx, remove_item=True)

    def _append(self, data_object, left_to_right=False):

        new_ref = Reference(data_object, None)
        new_ref.last_ref = None

        if self.head is None:
            self.head = self.tail = new_ref
        else:

            if left_to_right:
                new_ref.next_ref = self.head
                new_ref.last_ref = None
                self.head.last_ref = new_ref
                self.head = new_ref

            else:
                new_ref.last_ref = self.tail
                new_ref.next_ref = None
                self.tail.next_ref = new_ref
                self.tail = new_ref

    def append(self, data_object):

        self._append(data_object)
        del data_object

    def appendleft(self, data_object):

        self._append(data_object, True)
        del data_object

    def _extend(self, iterable, left_to_right=False):

        if not getattr(iterable, '__iter__'):
            raise TypeError('{} is not iterable'.format(type(iterable)))

        for data_object in iterable:
            self._append(data_object, left_to_right)
        del iterable

    def extend(self, iterable):

        self._extend(iterable)
        del iterable

    def extendleft(self, iterable):

        self._extend(iterable, True)
        del iterable

    def popright(self):

        return self._pop()

    def popleft(self):

        return self._pop(0)

    def copy(self):

        return DoubleLinkedList(self)

