class Reference:

    def __init__(self, value, next_ref=None, last_ref=None):
        self.value = value
        self.next_ref = next_ref
        self.last_ref = None
        if last_ref:
            self.last_ref = last_ref

    def __str__(self):
        return str(self.value)

    def __repr__(self):
        return self.reveal()

    def reveal(self):
        retstr = "("
        retstr += repr(self.last_ref.value) + " <- " if self.last_ref else ""
        retstr += repr(self.value)
        retstr += " -> "+repr(self.next_ref.value) if self.next_ref else ""
        retstr += ")"
        return retstr