from Reference import Reference


class SingleLinkedList:
    head = None
    tail = None

    def __init__(self, iterable=()):
        self._extend(iterable)

    def __str__(self):

        if not len(self):
            return '[]'
        current_ref = self.head
        retstr = '['

        while current_ref:
            retstr += repr(current_ref.value)+', '
            current_ref = current_ref.next_ref
        return retstr[:-2]+']'

    def __repr__(self):

        if not len(self):
            return '[]'
        current_ref = self.head
        retstr = '['

        while current_ref:
            retstr += repr(current_ref)+', '
            current_ref = current_ref.next_ref
        return retstr[:-2]+']'

    def __len__(self):
        current_ref = self.head
        length = 0

        while current_ref:
            length += 1
            current_ref = current_ref.next_ref
        return length

    def __getitem__(self, item_idx):

        if type(item_idx) != int:
            raise TypeError('list indices must be integer, not {}'.format(type(item_idx)))
        length = len(self)

        if (item_idx > length-1) or (item_idx < 0-length):
            raise IndexError('{} index out of range'.format(self.__class__.__name__))

        if item_idx < 0:
            item_idx += length
        del length
        current_ref = self.head
        i = 0

        while i != item_idx:
            current_ref = current_ref.next_ref
            i += 1
        return current_ref.value

    def __delitem__(self, item_idx):
        self._remove(item_idx)

    def __iter__(self):
        self.current = 0
        self.max = len(self)
        return self

    def __next__(self):

        if self.current == self.max:
            raise StopIteration
        else:
            self.current += 1
            return self[self.current-1]

    def _remove(self, item_idx, length=None):
        length = length if length else len(self)

        if type(item_idx) != int:
            raise TypeError('list indices must be integer, not {}'.format(type(item_idx)))

        if item_idx < 0:
            ins_idx = abs(item_idx + length)

        if (item_idx > length-1) or (item_idx < 0-length):
            raise IndexError('{} index out of range'.format(self.__class__.__name__))
        current_ref = self.head
        i = 0

        while i != item_idx:
            last_ref = current_ref
            current_ref = current_ref.next_ref
            next_ref = current_ref.next_ref
            i += 1
        data_object = current_ref.value

        if i == 0:
            self.head = current_ref.next_ref
        else:
            last_ref.next_ref = next_ref

        if i == length-1 and length > 1:
            self.tail = last_ref

        return data_object

    def _pop(self, item_idx=None):
        length = len(self)

        if item_idx is None:
            item_idx = length - 1

        if type(item_idx) != int:
            raise TypeError('list indices must be integer, not {}'.format(type(item_idx)))

        if not length:
            raise IndexError('pop from empty {}'.format(self.__class__.__name__))

        if 0 < item_idx > length-1 or (0 > item_idx and abs(item_idx) > length):
            raise IndexError('pop index out of range')

        return self._remove(item_idx, length)

    def append(self, data_object):

        new_ref = Reference(data_object, None)

        if self.head is None:
            self.head = self.tail = new_ref
        else:
            self.tail.next_ref = new_ref
            self.tail = new_ref

    def _append(self, data_object):

        new_ref = Reference(data_object, None)

        if self.head is None:
            self.head = self.tail = new_ref
        else:
            self.tail.next_ref = new_ref
            self.tail = new_ref

    def extend(self, iterable):

        if not getattr(iterable, '__iter__'):
            raise TypeError('{} object is not iterable'.format(type(iterable)))

        for data_object in iterable:
            self.append(data_object)
        del iterable

    def _extend(self, iterable):

        if not getattr(iterable, '__iter__'):
            raise TypeError('{} object is not iterable'.format(type(iterable)))

        for data_object in iterable:
            self._append(data_object)
        del iterable

    def clear(self):

        for item in range(len(self)):
            del self[0]
        self.head = None
        self.tail = None

    def pop(self, item_idx=None):

        return self._pop(item_idx)

    def popleft(self):

        return self._pop(0)

    def copy(self):

        return SingleLinkedList(self)