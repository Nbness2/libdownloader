from hashlib import sha512

import OPCODES
from test_packages.pymisc import misc
from test_packages_2.ppbu.BufferUtils import ByteBuffer


class File:

    def __init__(self, file_path, parent_id=-1, content_id=-1):

        self.file_name = file_path.split("\\")[-1][:127]
        self.chunks = []  # Chunks are up to 512 bytes
        self.parent_id = parent_id
        self.content_id = content_id

        with open(file_path, "rb") as file:
            file_data = file.read()

        self.file_hash = sha512(file_data).digest()
        self.chunks = [chunk for chunk in misc.chunk_iterable(file_data, 512)]
        self.init_packet_built = False
        self.last_packet_built = False
        self.chunk_counter = 0

    def __repr__(self):

        return "("+str(self.parent_id) + ", "+str(self.content_id)+")"

    def __getitem__(self, item):
        if type(item) is slice:
            item = item.start
        if type(item) < 0:
            item = 0
        return self.build_chunk_packet(item)

    def scan(self): pass

    def build_meta_packet(self):
        buffer = ByteBuffer()
        buffer.write_byte(OPCODES.Server.SEND_CONTENT_META)
        buffer.write_short(self.parent_id)
        buffer.write_short(self.content_id)
        buffer.write_byte(len(self.file_name))
        buffer.write_bytes(self.file_name.encode())
        buffer._write_arbitrary(data=len(self.chunks), forced_size=3)

    def build_chunk_packet(self, chunk_id):
        buffer = ByteBuffer()
        buffer.write_byte(OPCODES.Server.SEND_FILE_CHUNK)
        # S -> [opcode (1b), file_id (2b), chunk_id (3b), chunk_size (2b), chunk_contents (1-512b)] - 9 to 520 bytes
        buffer.write_short(self.content_id)
        buffer._write_arbitrary(data=chunk_id, forced_size=3)
        buffer.write_short(len(self.chunks[chunk_id]))
        buffer.write_bytes(self.chunks[chunk_id].encode())
