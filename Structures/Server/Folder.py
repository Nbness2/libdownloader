import os

import OPCODES
from Structures.Server.File import File
from test_packages_2.ppbu.BufferUtils import ByteBuffer


class Folder:

    def __init__(self, folder_path, parent_id=-1, content_counter=[0], *, ignore_folders=(), ignore_extensions=()):

        self.folder_path = folder_path
        self.folder_name = folder_path.split("\\")[-1][:127]
        self.contents = []
        self.flattened_contents = []
        self.ignore_folders = ignore_folders
        self.ignore_extensions = ignore_extensions
        self.parent_id = parent_id
        self.content_counter = content_counter
        self.content_id = content_counter[0]

    def __str__(self):

        return "["+str(self.parent_id) + ", "+str(self.content_id)+"]"

    def __repr__(self):

        return "["+str(self.parent_id) + ", "+str(self.content_id)+"]"

    def __getitem__(self, item):
        return self.build_meta_packet()

    def request_flatten(self):

        flattened = []

        for content in self.contents:
            flattened.append(content)

        for content in flattened:
            if type(content) is Folder:
                content.request_flatten()
                flattened.extend(content.flattened_contents)

        self.flattened_contents = tuple(flattened)

    def add_folder(self, folder_name):

        self.contents.append(
            Folder(
                self.folder_path + "\\" + folder_name,
                parent_id=self.content_id,
                content_counter=self.content_counter,
                ignore_folders=self.ignore_folders,
                ignore_extensions=self.ignore_extensions
            )
        )

        self.content_counter[0] += 1

    def add_file(self, file_name):

        self.contents.append(
            File(
                file_path=self.folder_path + "\\" + file_name,
                parent_id=self.content_id,
                content_id=self.content_counter[0]
            )
        )
        self.content_counter[0] += 1

    def scan(self):

        for path, folders, files in os.walk(self.folder_path):

            if self.ignore_folders:

                for folder_name in self.ignore_folders:

                    if folder_name in folders:
                        folders.remove(folder_name)

            if self.ignore_extensions:
                for extension in self.ignore_extensions:

                    for file_name in files:

                        if extension in file_name:
                            files.remove(file_name)
                            break

            for folder_name in folders:
                self.add_folder(folder_name)

            for file_name in files:
                self.add_file(file_name)

            break

        for content in self.contents:
            if type(content) is Folder:
                content.scan()
                content.request_flatten()

    def build_meta_packet(self):
        buffer = ByteBuffer()
        buffer.write_byte(OPCODES.Server.SEND_CONTENT_META)
        buffer.write_short(self.parent_id)
        buffer.write_short(self.content_id)
        buffer.write_byte(len(self.folder_name))
        buffer.write_bytes(self.folder_name.encode())
        buffer._write_arbitrary(data=0, forced_size=3)
