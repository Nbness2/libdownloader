import os

import OPCODES
from Structures.Server.File import File
from Structures.Server.Folder import Folder
from test_packages.pymisc.misc import chunk_iterable
from test_packages_2.ppbu.BufferUtils import ByteBuffer


class Package:

    def __init__(self, package_name, package_path):

        self.package_name = package_name[:127]  # The default name for calling the package
        self.package_path = package_path  # The absolute path of the package "root" e.g. "C:\Users\Me\Python\Packages"
        self.ignore_folders = None
        self.ignore_extensions = None
        self.description = None

        with open(package_path+"\\package_config.cfg", "r") as config_file:
            config_data = config_file.readlines()

        self.read_config(config_data)
        self.contents = []
        self.flattened_contents = []
        self.content_id = 0
        self.content_counter = [1]  # currently too tired to think of a way to link this throughout the whole thing in the way i want

    def __str__(self):
        return "package: " + self.package_name

    def __getitem__(self, item):
        content = self.flattened_contents[item]
        if type(item) is slice and type(content) is File:
            return content[item.stop]
        return self.flattened_contents[item]

    def flatten_contents(self):
        flattened = []

        for content in self.contents:
            if type(content) is Folder:
                content.request_flatten()
            flattened.append(content)

        for content in flattened:
            if type(content) is Folder:
                for subcontent in content.flattened_contents:
                    if subcontent.content_id-1 == len(flattened):
                        flattened.append(subcontent)

        self.flattened_contents = tuple(flattened)

    def read_config(self, data):
        for line in data:
            config_name, config_value = line.strip().split("=")
            config_name = config_name.strip()
            config_value = config_value.strip()

            if config_name.lower() == "ignore_folders":

                if config_value == "[]":
                    self.ignore_folders = []
                    continue

                folder_names = config_value.split("\"")

                for value in folder_names:

                    if len(value) < 3 and value in ("[", "[ ", " [", "]", "] ", " ]", ",", ", ", " ,"):
                        folder_names.remove(value)

                self.ignore_folders = folder_names

            if config_name.lower() == "ignore_extensions":

                if config_value == "[]":
                    self.ignore_extensions = []
                    continue

                extensions = config_value.split("\"")

                for value in extensions:

                    if len(value) < 3 and value in ("[", "[ ", " [", "]", "] ", " ]", ",", ", ", " ,"):
                        extensions.remove(value)

                self.ignore_extensions = extensions

            if config_name.lower() == "description":

                if config_value == "":
                    self.description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
                    continue

                self.description = [chunk for chunk in chunk_iterable(config_value[1:-1], 512)]


        #[chunk for chunk in misc.chunk_iterable(description.encode())] if description else [b"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."]
        pass

    def add_folder(self, folder_name):

        self.contents.append(
            Folder(
                self.package_path + "\\" + folder_name,
                parent_id=self.content_id,
                content_counter=self.content_counter,
                ignore_folders=self.ignore_folders,
                ignore_extensions=self.ignore_extensions
            )
        )

        self.content_counter[0] += 1

    def add_file(self, file_name):

        self.contents.append(
            File(
                file_path=self.package_path + "\\" + file_name,
                parent_id=self.content_id,
                content_id=self.content_counter[0]
            )
        )

        self.content_counter[0] += 1

    def scan(self):

        for path, folders, files in os.walk(self.package_path):

            if self.ignore_folders:

                for folder_name in self.ignore_folders:

                    if folder_name in folders:
                        folders.remove(folder_name)

            if self.ignore_extensions:
                for extension in self.ignore_extensions:

                    for file_name in files:

                        if extension in file_name:
                            files.remove(file_name)
                            break

            for folder_name in folders:
                self.add_folder(folder_name)

            for file_name in files:
                self.add_file(file_name)

            break

        for content in self.contents:
            if type(content) is Folder:
                content.scan()
                content.request_flatten()

    def build_meta_packet(self):

        buffer = ByteBuffer()
        buffer.write_byte(OPCODES.Server.SEND_PKG_META)
        buffer.write_short(len(self.description))
        buffer.write_short(self.content_counter[0])
        return buffer.get_buffer()

    def build_desc_chunk_packet(self, chunk_id):

        buffer = ByteBuffer()
        buffer.write_byte(OPCODES.Server.SEND_DESC_CHUNK)
        buffer.write_short(chunk_id)
        buffer.write_short(len(self.description[chunk_id]))
        buffer.write_bytes(self.description[chunk_id].encode())
        return buffer.get_buffer()
