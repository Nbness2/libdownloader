# Client and Server opcodes


class Client:
    CONFIRM_STATUS = 0
    # S -> [opcode (1b), status_type (1b)] - 2 bytes

    REQUEST_PKG_META = 1
    # S -> [opcode (1b), package_name_size (1b), package_name (1-127b)] - 3 to 129 bytes

    REQUEST_DESC_CHUNK = 2
    # S -> [opcode (1b), description_chunk_id (2b)] - 3 bytes

    REQUEST_CONTENT_META = 3
    # S -> [opcode (1b), file_id (2b)] - 3 bytes

    REQUEST_FILE_CHUNK = 4
    # S -> [opcode (1b), file_id (2b), chunk_id (3b)]

    COMPARE_FILE_HASH = 5
    # S -> [opcode (1b), file_id (2b), file_hash (128b)] - 131 bytes


class Server:
    CONFIRM_STATUS = 0
    # S -> [opcode (1b), status_type (1b)] - 2 bytes
    # TODO: params

    DENY_STATUS = 1
    # S -> [opcode (1b), status_type (1b), status_reason (1b)] - 3 bytes
    # TODO: params

    SEND_PKG_META = 2
    # S -> [opcode (1b), description_chunk_amount (2b), file_count (2b)] - 3 bytes

    SEND_DESC_CHUNK = 3
    # S -> [opcode (1b), chunk_id (2b), chunk_length (2b), chunk_contents (1-512b)] - 6 to 517 bytes

    SEND_CONTENT_META = 4
    # S -> [opcode (1b), parent_id (2b), content_id (2b), file_name_length (1b), file_name (1-127b), chunk_amount (3b)] - 10 to 136 bytes

    SEND_FILE_CHUNK = 5
    # S -> [opcode (1b), file_id (2b), chunk_id (3b), chunk_size (2b), chunk_contents (1-512b)] - 9 to 520 bytes

